import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSlideshowAcracodeComponent } from './ngx-slideshow-acracode.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  declarations: [
    NgxSlideshowAcracodeComponent,
  ],
  exports: [
    NgxSlideshowAcracodeComponent,
  ]
})
export class NgxSlideshowAcracodeModule { }
