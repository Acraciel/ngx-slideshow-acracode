import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
  keyframes
} from '@angular/animations';
import { NgxSlideshowAcracodeModel } from './ngx-slideshow-acracode.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ngx-slideshow-acracode',
  templateUrl: './ngx-slideshow-acracode.component.html',
  styleUrls: ['./ngx-slideshow-acracode.component.scss'],
  animations: [
    trigger('move', [
      state('in',
        style({ transition: ('0') })
      ),
      transition('void => left', [
        style({ transition: ('100%') }),
        animate('800ms', keyframes ([
          style({ opacity: 1}),
          style({ opacity: 0})
        ]))
      ]),
      transition('left => void', [
        style({ transition: ('0') }),
        animate('800ms', keyframes ([
          style({ opacity: 0}),
          style({ opacity: 1})
        ]))
      ]),
      transition('void => right', [
        style({ transition: ('-100%') }),
        animate('800ms', keyframes ([
          style({ opacity: 1}),
          style({ opacity: 0})
        ]))
      ]),
      transition('right => void', [
        style({ transition: ('0') }),
        animate('800ms', keyframes ([
          style({ opacity: 0}),
          style({ opacity: 1})
        ]))
      ])
    ])
  ]
})
export class NgxSlideshowAcracodeComponent implements OnInit {
  @Input() images: [NgxSlideshowAcracodeModel];
  @Input() auto: boolean;
  @Input() timeLap: number;
  @Input() public imageFit: string;
  @Input() public buttonSize: string;
  @Input() public buttonPosition: string;
  @Output() public clickedButton: EventEmitter<any> = new EventEmitter<any>();

  public imageUrls: [NgxSlideshowAcracodeModel];
  public state = 'void';
  public disableSliderButtons = false;

  constructor() {
  }

  ngOnInit() {
    this.imageUrls = this.images;
    this.timeLap = this.timeLap ? this.timeLap : 6000;
    if (this.auto) {
      setInterval(() => this.moveRight(), this.timeLap);
    }
  }

  imageRotate(arr, reverse) {
    if (reverse) {
      arr.unshift(arr.pop());
    } else {
      arr.push(arr.shift());
    }
    return arr;
  }

  moveLeft() {
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'right';
    this.imageRotate(this.imageUrls, true);
  }

  moveRight() {
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'left';
    this.imageRotate(this.imageUrls, false);
  }

  onFinish($event) {
    this.state = 'void';
    this.disableSliderButtons = false;
  }

  onStart($event) {
    this.disableSliderButtons = true;
  }

  onClickButton (item) {
    this.clickedButton.emit(item);
  }

}
