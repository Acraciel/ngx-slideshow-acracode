
export class NgxSlideshowAcracodeModel {
    private url: string;
    private btnText: string;
    private rotate: string;
    /**
     * @author Rodrigo Rojas S.
     * @description Modelo que inicializará un arreglo de imágenes
     * @param url define la url de la imágen
     * @param btnText define el texto que tendrá el botón
     * @param rotate define si la imagen se debe girar debe contener el texto ej: "rotate(90deg)"
     */
    constructor (url: string, btnText?: string, rotate?: string) {
        this.url = url;
        this.btnText = btnText;
        this.rotate = rotate;
    }
}
