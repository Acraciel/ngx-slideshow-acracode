# ngx-slideshow-acracode
A slideshow component for Angular 2+

## Install

```javascript
npm install ngx-slideshow-acracode --save
```

## Component
- Selector：`ngx-slideshow-acracode`

- Module:  `NgxSlideshowAcracodeModule`

- Model: `NgxSlideshowAcracodeModel`

```javascript
import { NgxSlideshowAcracodeModule } from 'ngx-slideshow-acracode';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgxSlideshowAcracodeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

- Example use:

```javascript
import { NgxSlideshowAcracodeModel } from 'ngx-slideshow-acracode';


imagesUrl = [
    new NgxSlideshowAcracodeModel('https://bmj2k.files.wordpress.com/2011/04/heroes.jpg', 'Button 1'),
    new NgxSlideshowAcracodeModel('https://www.cars.co.za/carimages_gen/Audi-TT/Audi-TT-coupe-1.8TFSI_AudiTT3c6l.jpg', 'Button 2', 'rotate(90deg)'),
    new NgxSlideshowAcracodeModel('http://comicsalliance.com/files/2011/04/strips02.jpg', 'Button 3', 'rotate(180deg)')
];
// Handle event when clicked one button
clicked($event) {
    alert('Tú url es: ' + $event.url);
}
```

- Selector

```html
<ngx-slideshow-acracode></ngx-slideshow-acracode>
```

## Inputs
- `images` Arreglo de `NgxSlideshowAcracodeModel`que nos entrega las imágenes que usaremos.
- `auto` Boolean que nos indica si el slideshow estará automático
- `timeLap` En milisegundos, cada cuanto irá cambiando el slide
- `imageFit` Propiedad que hará que las imágenes esten en estado [contain, cover, fill]
- `buttonSize` Edita el tamaño del botón [medium ,large] (defecto normal)
- `buttonPosition` Edita la posición del botón [left, right] (defecto normal)

## Output

- `clickedButton` Evento que nos retornará un `NgxSlideshowAcracodeModel`del item que se está mostrando.

**Example of use:**

```html
<ngx-slideshow-acracode 
   [images]="imagesUrl" 
   [auto]="true" 
   [timeLap]="6000"
   (clickedButton)="clicked($event)">
</ngx-slideshow-acracode>

```

## Models
- `events` = `NgxSlideshowAcracodeModel` - Datos de las imágenes
  - url: string; `Url de la imagen`
  - btnText?: string; `Texto del botón`
  - rotate?: string; `Se basa en la rotación de imágenes de css transform por lo que debe incluir los string de la siguiente forma: 'rotate(90deg)', cambiando el numero por los grados que deseamos girar la imagen.`

# Issues report

https://bitbucket.org/Acraciel/ngx-slideshow-acracode/issues

# License
Released under the MIT License. See the [LICENSE](license) file for further details.